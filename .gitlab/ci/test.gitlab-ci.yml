###############################################
#              Test the website               #
###############################################

#
# Test the links in the global nav for all MRs in `gitlab-docs`
#
test_global_nav_links:
  image: registry.gitlab.com/gitlab-org/gitlab-docs/lint-html:alpine-3.18-ruby-3.2.2-4207821e
  extends:
    - .rules_site_tests
    - .bundle
  stage: test
  script:
    # Only check files in top-level directories. This means a much faster and less repetitive check of global navigation links.
    - echo "  all:" >> nanoc.yaml
    - echo "    exclude_files:" >> nanoc.yaml
    - echo "      - '\/(ee|runner|omnibus|charts|operator)\/.*\/.*'" >> nanoc.yaml
    - "parallel time bundle exec nanoc check ::: internal_links internal_anchors"

#
# Check the redirect file for duplicates
#
check_duplicate_redirects:
  image: busybox
  extends:
    - .rules_site_tests
  needs: []
  stage: test
  script:
    - grep -Ir "  - from:" content/_data/redirects.yaml | sort | uniq -d | tee output.txt
    - exit $(cat output.txt | wc -l)

#
# Check global navigation for:
#
# - Identical duplicate entries.
# - index.html in the entries.
# - Entries that don't conform to the schema.
#
check_global_nav_entries:
  extends:
    - .rules_site_tests
    - .bundle
  needs: []
  stage: test
  script:
    - make check-global-navigation

#
# Check that asdf dependencies install correctly
#
check_asdf_installation:
  image: ubuntu:22.04
  variables:
    ASDF_VERSION: '0.11.1'
  needs: []
  stage: test
  before_script:
    - apt update
    - apt install -y build-essential git curl zlib1g-dev libyaml-dev libssl-dev
  script:
    - git clone https://github.com/asdf-vm/asdf.git ~/.asdf --branch v$ASDF_VERSION
    - source "$HOME/.asdf/asdf.sh"
    - make setup-asdf
    - make install-asdf-dependencies
  rules:
    - if: '$DOCS_PROJECT_PIPELINE_TYPE =~ /^MR pipeline.*/'
      changes:
        - .tool-versions

#
# Run rspec tests
#
rspec:
  extends:
    - .rules_site_tests
    - .bundle_and_yarn
  needs: []
  stage: test
  script:
    - make rspec-tests
  coverage: '/\(\d+.\d+\%\) covered/'
  artifacts:
    reports:
      coverage_report:
        coverage_format: cobertura
        path: coverage/coverage.xml

#
# Run JavaScript tests
#
jest:
  extends:
    - .rules_site_tests
    - .yarn
  needs: []
  stage: test
  script:
    - make jest-tests

#
# Lint JavaScript
#
js_lint:
  extends:
    - .rules_site_tests
    - .yarn
  needs: []
  stage: test
  script:
    - make eslint-tests
    - make prettier-tests

#
# RuboCop
#
rubocop:
  extends:
    - .rules_site_tests
    - .bundle
  needs: []
  stage: test
  script:
    - make rubocop-tests

#
# Lint SCSS
#
stylelint:
  extends:
    - .rules_site_tests
    - .yarn
  needs: []
  stage: test
  script:
    - make stylelint-tests

#
# Yamllint of *.yml for .gitlab-ci.yml.
# This uses rules from project root `.yamllint`.
#
yamllint:
  extends:
    - .rules_site_tests
  needs: []
  stage: test
  script:
    - apk add yamllint
    - make yamllint-tests

#
# Run markdownlint tests
#
markdownlint:
  extends:
    - .rules_site_tests
    - .yarn
  needs: []
  stage: test
  script:
    - make markdownlint-tests

#
# Run ShellCheck tests
#
shellcheck:
  extends:
    - .rules_site_tests
  image: koalaman/shellcheck-alpine:v0.9.0
  needs: []
  stage: test
  script:
    - apk add make
    - make shellcheck-tests

#
# Check for broken external links from the GitLab project
#
test_external_links_gitlab:
  extends:
    - .bundle
    - .rules_chores
  stage: test
  script:
    - echo "  all:" >> nanoc.yaml
    - echo "    exclude_files:" >> nanoc.yaml
    - echo "      - '\/(runner|omnibus|charts|operator)\/.*\/.*'" >> nanoc.yaml
    - bundle exec nanoc check external_links

#
# Check for broken external links from the Omnibus GitLab project
#
test_external_links_omnibus_gitlab:
  extends:
    - .bundle
    - .rules_chores
  stage: test
  script:
    - echo "  all:" >> nanoc.yaml
    - echo "    exclude_files:" >> nanoc.yaml
    - echo "      - '\/(ee|runner|charts|operator)\/.*\/.*'" >> nanoc.yaml
    - bundle exec nanoc check external_links

#
# Check for broken external links from the GitLab Runner project
#
test_external_links_gitlab_runner:
  extends:
    - .bundle
    - .rules_chores
  stage: test
  script:
    - echo "  all:" >> nanoc.yaml
    - echo "    exclude_files:" >> nanoc.yaml
    - echo "      - '\/(ee|omnibus|charts|operator)\/.*\/.*'" >> nanoc.yaml
    - bundle exec nanoc check external_links

#
# Check for broken external links from the GitLab Chart project
#
test_external_links_charts:
  extends:
    - .bundle
    - .rules_chores
  stage: test
  script:
    - echo "  all:" >> nanoc.yaml
    - echo "    exclude_files:" >> nanoc.yaml
    - echo "      - '\/(ee|runner|omnibus|operator)\/.*\/.*'" >> nanoc.yaml
    - bundle exec nanoc check external_links

#
# Check for broken external links from the GitLab Operator project
#
test_external_links_operator:
  extends:
    - .bundle
    - .rules_chores
  stage: test
  script:
    - echo "  all:" >> nanoc.yaml
    - echo "    exclude_files:" >> nanoc.yaml
    - echo "      - '\/(ee|runner|omnibus|charts)\/.*\/.*'" >> nanoc.yaml
    - bundle exec nanoc check external_links

#
# Run markdownlint to find EOL whitespace to clean up
#
test_EOL_whitespace:
  extends:
    - .rules_chores
    - .bundle
  stage: test
  image: registry.gitlab.com/gitlab-org/gitlab-docs/lint-html:alpine-3.18-ruby-3.2.2-4207821e
  needs: []
  before_script: []
  dependencies: []
  script:
    - apk add jq
    - bundle config set --local path 'vendor/bundle'  # Install dependencies into ./vendor/bundle
    - bundle install
    - bundle exec rake default
    - yarn global add markdownlint-cli2
    - make markdownlint-whitespace-tests

test_unlinked_images:
  extends:
    - .rules_chores
    - .bundle
  stage: test
  image: registry.gitlab.com/gitlab-org/gitlab-docs/lint-html:alpine-3.18-ruby-3.2.2-4207821e
  needs: []
  before_script: []
  dependencies: []
  script:
    - apk add jq
    - bundle config set --local path 'vendor/bundle'  # Install dependencies into ./vendor/bundle
    - bundle install
    - bundle exec rake default
    - cp tasks/unlinked-images.sh ../gitlab/unlinked-images.sh
    - cp tasks/unlinked-images.sh ../charts-gitlab/unlinked-images.sh
    - cp tasks/unlinked-images.sh ../omnibus-gitlab/unlinked-images.sh
    - cp tasks/unlinked-images.sh ../gitlab-runner/unlinked-images.sh
    - cd ../omnibus-gitlab
    - ./unlinked-images.sh doc
    - cd ../charts-gitlab
    - ./unlinked-images.sh doc
    - cd ../gitlab-runner
    - ./unlinked-images.sh docs
    - cd ../gitlab
    - ./unlinked-images.sh doc

lint_dockerfiles:
  extends:
    - .rules_site_tests
  image: hadolint/hadolint:latest-alpine
  needs: []
  dependencies: []
  script:
    - apk add make
    - make hadolint-tests

#
# Untamper job overrides
#
untamper-my-lockfile:
  rules:
    - if: '$DOCS_PROJECT_PIPELINE_TYPE =~ /^MR pipeline.*/'
      changes:
        - yarn.lock
